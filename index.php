<html>
    <head>
        <meta charset="UTF-8">
        <title>Registro de empleados</title>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <script src="js/jquery_v3.3.1.js"></script>
        <script src="js/ajax.js"></script>
        <style>
            .editar{cursor:pointer; color:green !important;}
        </style>
    </head>
    <body>
        <div id="contenedor">
            <form class="inscribir widget" id="inscribir" style="display:none;" onsubmit="return false;">
                <h4 class="widgettitulo"></h4>
                <input type="hidden" name="id" />
                <input type="text" name="nombre" placeholder="NOMBRE" autocomplete="off" tabindex="1" required>
                <input type="text" name="email" placeholder="E-MAIL" autocomplete="off" tabindex="2" required>
                <input type="text" name="telefono" placeholder="TELEFONO" autocomplete="off" tabindex="4" required>
                <input type="text" name="comentario" placeholder="Escriba comentario corto" tabindex="4" required>
                <button id="envio" tabindex="7">Guardar datos</button>
                <br style="clear:both;">
            </form>
            <section id="consultar" class="widget">
                <h4 class="widgettitulo">Listado de Empleados</h4>
                <div class="datagrid" id="datagrid"></div>  
            </section>
        </div> 
        <button onclick="nuevo_empleado()">Nuevo empleado</button>
    </body>
</html>
